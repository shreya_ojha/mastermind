import requests as rq
import json
sess = rq.Session()
mm_url = "https://we6.talentsprint.com/wordle/game/"
register_url = mm_url + "register"
create_url = mm_url + "create"
guess_url = mm_url + "guess"


#registering with json
def register(url: str) -> str:
    register_dict = {"mode": "Mastermind", "name": "team_bot"}
    register_with = json.dumps(register_dict)
    r = rq.post(url, json=register_dict)
    resp = sess.post(url, json=register_dict)
    return resp.json()['id']

id = register(register_url)

def create(id: str):
    create_dict = {"id": id, "overwrite": True}
    rc = rq.post(create_url, json=create_dict)
    
# Function to load words from a file
def load_words(filename):
    with open(filename, 'r') as file:
        words = [line.strip() for line in file if len(line.strip()) == 5]
    return words

# Load words from 'words.txt'
words = load_words('5letters.txt')
    resp = sess.post(create_url, json=create_dict)
    #me = resp.json()['id']
    return resp.json()

create(id)

def guess(word):
        guess_dict = {"guess":word, "id":id}
        res = sess.post(guess_url, json = guess_dict)
        return res.text

while (rq.Response() != "win"):
    word = input()
    #guess(word)
    print(guess(word))

